#!/bin/bash

GI_VERSION="v1.2.5"

GI_INSTALL_LOC="$HOME/GeraltInstaller"
GI_INSTALL_MANAGER="GeraltInstaller-Manager.sh"

SETUP_LOC="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

NC=`tput sgr0`
F_BLACK=`tput setaf 0`
B_BLACK=`tput setab 0`
F_WHITE=`tput setaf 7`
B_WHITE=`tput setab 7`
F_RED=`tput setaf 1`
B_RED=`tput setab 1`
F_GREEN=`tput setaf 2`
B_GREEN=`tput setab 2`
F_YELLOW=`tput setaf 3`
B_YELLOW=`tput setab 3`

MANAGER_UPDATE=0

# If the GI-Installation-Manager.sh exists, move it into the GeraltInstaller root folder.
cls() {
	clear && clear && clear
}
cls

echo "Detecting..."
if [ -e "$GI_INSTALL_LOC/$GI_INSTALL_MANAGER" ]; then
	echo "[  ${F_GREEN}OK${NC}  ] $GI_INSTALL_MANAGER found."
	
	if [ -e "$SETUP_LOC/$GI_INSTALL_MANAGER" ]; then
		echo "[  ${F_GREEN}OK${NC}  ] Newer installer found."
		
		echo ""
		echo "--- REMOVING OLD ---"
		echo ""
		
		rm -rf $GI_INSTALL_LOC/$GI_INSTALL_MANAGER
		
		if [ ! -e "$GI_INSTALL_LOC/$GI_INSTALL_MANAGER" ]; then	
			echo "[  ${F_GREEN}OK${NC}  ] Successfully removed."
		else
			MANAGER_UPDATE=1
			echo "[${F_RED}Failed${NC}] NOT Successfully removed."
		fi
		
		echo ""
		echo "--- MOVING NEW ---"
		echo ""
		
		mv $SETUP_LOC/$GI_INSTALL_MANAGER $GI_INSTALL_LOC/$GI_INSTALL_MANAGER
		
		if [ -e "$GI_INSTALL_LOC/$GI_INSTALL_MANAGER" ]; then	
			echo "[  ${F_GREEN}OK${NC}  ] Successfully moved."
		else
			echo "[${F_RED}Failed${NC}] NOT Successfully moved."
		fi
	else
		echo "[${F_RED}Failed${NC}] Installer NOT present."
		echo ""
		echo "Exiting installation..."
		exit
	fi
else
	echo "[${F_RED}Failed${NC}] $GI_INSTALL_MANAGER NOT found."
	echo ""
	echo "--- MOVING NEW ---"
	echo ""
	
	mv $SETUP_LOC/$GI_INSTALL_MANAGER $GI_INSTALL_LOC/$GI_INSTALL_MANAGER
	
	if [ -e "$GI_INSTALL_LOC/$GI_INSTALL_MANAGER" ]; then	
		echo "[  ${F_GREEN}OK${NC}  ] Successfully moved."
	else
		echo "[${F_RED}Failed${NC}] NOT Successfully moved."
	fi
fi

echo ""
echo "Install Manager Version Checking: ${F_GREEN}COMPLETE${NC}"
echo ""

countdown_3(){
	echo -n "3 " && sleep 1s
	echo -n "2 " && sleep 1s
	echo -n "1 " && sleep 1s
	echo ""
}

echo -n "Starting in..."
countdown_3
echo""

# Creates all the folder(s) used by GeraltInstaller
createFolderStructure() {
	echo ""
}

echo ""
echo "INSTALLER CALLED"
echo ""
exit 1

