#!/bin/bash

# This can be changed to the most current URL and release.
GIT_URL="https://gitlab.com/r00thkr/GeraltInstaller.git"
CURRENT_VERSION="v1.3.0"

# [ABOUT]
# This is a script installer for a Git installation program.
#
# [NOTE]
# The numbers by each of the if statements in the script are
# for numbering outer to inner 'if-else' condition statements
# when things start getting longer and further apart to make 
# changes easier to read and follow each closing statement.
# 
# [WARNING]
# A known problem with this script is that folder name 
# cannot have spaces or it will break the script.

NC=`tput sgr0`
F_BLACK=`tput setaf 0`
B_BLACK=`tput setab 0`
F_WHITE=`tput setaf 7`
B_WHITE=`tput setab 7`
F_RED=`tput setaf 1`
B_RED=`tput setab 1`
F_GREEN=`tput setaf 2`
B_GREEN=`tput setab 2`
F_YELLOW=`tput setaf 3`
B_YELLOW=`tput setab 3`

INSTALL_LOC="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
INSTALLER_SCRIPT="GeraltInstaller-Manager.sh"
INSTALLER_SCRIPT_LOCAL="$INSTALL_LOC/$INSTALLER_SCRIPT"
INSTALLER_SCRIPT_TMP="/tmp/$INSTALLER_SCRIPT"

####################################
#                                  #
#    TMP LOCATION FOR INSTALLER    #
#                                  #
####################################
install_manager_tmp_in() {
	if [ -e "$INSTALLER_SCRIPT_LOCAL" ]; then
		mv $INSTALLER_SCRIPT_LOCAL $INSTALLER_SCRIPT_TMP
	fi
}

install_manager_tmp_out() {
	if [ -e "$INSTALLER_SCRIPT_TMP" ]; then
		mv $INSTALLER_SCRIPT_TMP $INSTALLER_SCRIPT_LOCAL
	fi
}

#############################################
#                                           #
#    PRE-CONFIGURATION AND AUTHORIZATION    #
#                                           #
#############################################
GI_VERSION="NONE"
PRE_INSTALL_CHECK=""
SYSTEM_CHECK=""

no_privileges() {
	echo ""
	echo "You don't have sufficient privileges to run this script."
	echo "Try again with sudo..."
	echo ""
}

arg_to_var() {
	# Condition check for auto-run installer
	if [ "$1" == "-a" ]; then
		PRE_INSTALL_CHECK=$1
	elif [ "$2" == "-a" ]; then
		PRE_INSTALL_CHECK=$2
	elif [ "$3" == "-a" ]; then
		PRE_INSTALL_CHECK=$3
	else
		PRE_INSTALL_CHECK="NONE"
	fi
	
	# Condition to check or an exising installtion and present options.
	if [ "$1" == "-i" ]; then
		SYSTEM_CHECK="Yes"
	elif [ "$2" == "-i" ]; then
		SYSTEM_CHECK="Yes"
	elif [ "$3" == "-i" ]; then
		SYSTEM_CHECK="Yes"
	else
		SYSTEM_CHECK="No"
	fi
}

arg_check() {
	arg_to_var $1 $2 $3
	
	# Default is "NONE", this will automatically use the latest version.
	if [ "$GI_VERSION" == "NONE" ]; then
		GI_VERSION=$CURRENT_VERSION
	fi
}

auth_check() {
	if [ "$(whoami)" != "root" ]; then
		no_privileges
		exit 1
	else
		arg_check $1 $2 $3
	fi
}

auth_check $1 $2 $3
install_manager_tmp_in

##########################
#                        #
#    SCRIPT VARIABLES    #
#                        #
##########################
GI_REPO="$INSTALL_LOC/GeraltInstaller"
GI_REPO_VERSION="$GI_REPO/$GI_VERSION"
GI_SETUP="runSetup.sh"
GI_LOC="$HOME/GeraltInstaller"
GI_LOCAL_VERSION="$GI_LOC/$GI_VERSION"
GI_INSTALLER="$GI_LOCAL_VERSION/$GI_SETUP"

MENU_OPTIONS=("Yes" "No" "Maybe")
INNER_MENU_OPTIONS=("Yes" "No")

TIMESTAMP=$(date -u)

##########################
#                        #
#    SCRIPT FUNCTIONS    #
#                        #
##########################
what_is_setup() {
	echo "Setup is..."
}

pause_bash(){
   read -p "$*"
}

pause() {
	pause_bash 'Press [Enter] key to continue...'
}

cls() {
	clear && clear && clear
}

gi_dirs() {
	mkdir $HOME/GeraltInstaller
	cp -r $GI_REPO_VERSION $GI_LOC
}

clean_up() {
	rm -r $INSTALL_LOC/*
}

clean_local() {
	rm -r $GI_LOC
}

repo_fail() {
	clean_up
	echo "[${F_RED}Failed${NC}] File/Folder not found..."
	echo ""
}

local_fail() {
	if [ "$1" == "gi_home" ]; then
		echo "[${F_RED}Failed${NC}] Local 'GeraltInstaller' folder missing..."
		echo ""
		echo "No local home folder found."
	elif [ "$1" == "gi_version" ]; then
		echo "[${F_RED}Failed${NC}] Local '$GI_VERSION' folder missing..."
		echo ""
		echo "No local version folder found."
	elif [ "$1" == "gi_setup" ]; then
		echo "[${F_RED}Failed${NC}] Local '$GI_SETUP' file missing..."
		echo ""
		echo "No local setup file found."
	else
		echo "[${F_RED}Failed${NC}] Local files missing..."
		echo ""
		echo "Local file(s) missing."
	fi
	
	echo ""
	echo "Cleaning..."
	echo ""
	clean_local
	clean_up
	install_manager_tmp_out
	echo "${F_YELLOW}FINISHED${NC}"
	exit 1
}

##############################
#                            #
#    SYSTEM CHECK REQUEST    #
#                            #
##############################
installation_check() {
	if [ "$SYSTEM_CHECK" == "Yes" ]; then
		cls
		echo "$TIMESTAMP"
		echo ""
		echo "Checking system for GeraltInstaller..."
	
		if [ -d "$GI_LOC" ]; then
		#	echo "[  ${F_GREEN}OK${NC}  ]"
			
			if [ -d "$GI_LOCAL_VERSION" ]; then
			#	echo "[  ${F_GREEN}OK${NC}  ]"
				echo ""
			
				echo "[  ${F_GREEN}DETECTED${NC}  ] ~/GeraltInstaller"
				echo "[  ${F_GREEN}DETECTED${NC}  ] ~/GeraltInstaller/$GI_VERSION"
				echo ""
				echo "Remove detected folders/files?"
				echo ""
				select MENU_CHOICE in "${INNER_MENU_OPTIONS[@]}"
				do
					case $MENU_CHOICE in
						"Yes" )
							clean_local
							install_manager_tmp_out
							echo ""
							echo "[ ${F_GREEN}Done${NC} ]"
							echo ""
							exit 1
							break;;
						"No" )
							install_manager_tmp_out
							echo ""
							echo "Exiting..."
							echo ""
							exit 1
							break;;
					esac
				done
				
			else
				echo ""
				echo "[${F_RED}NOT DETECTED${NC}] $GI_VERSION"
				echo "[  ${F_GREEN}DETECTED${NC}  ] $GI_LOC"
				echo ""
				
				echo "Remove these folder(s)/file(s)?"
				echo ""
				select MENU_CHOICE in "${INNER_MENU_OPTIONS[@]}"
				do
					case $MENU_CHOICE in
						"Yes" )
							clean_local
							install_manager_tmp_out
							echo ""
							echo "[ ${F_GREEN}Done${NC} ]"
							echo ""
							exit 1
							break;;
						"No" )
							install_manager_tmp_out
							echo ""
							echo "Exiting..."
							echo ""
							exit 1
							break;;
					esac
				done
			fi
		else
			install_manager_tmp_out
			echo "[${F_RED}Failed${NC}] GeraltInstaller not found."
			echo ""
			echo "Nothing detected locally."
			echo ""
			exit 1
		fi
	fi
}
installation_check

####################################
#                                  #
#    EXECUTION AND SCRIPT LOGIC    #
#                                  #
####################################
INSTALLATION_MENU_01=("Remove Install" "Info" "Exit")
INSTALLATION_MENU_02=("Install" "Info" "Exit")

get_git() {
	echo "Version: ${F_YELLOW}$GI_VERSION${NC}"
	echo "Attempting download... "
	echo ""
	
	git clone $GIT_URL
	if [ -e "$GI_REPO/$INSTALLER_SCRIPT" ]; then
		rm $GI_REPO/$INSTALLER_SCRIPT
		install_manager_tmp_out
	fi
}

what_is_manager_01() {
	echo "Manager 1..."
}

gi_installation_manager_01() {
	cls
	echo "$TIMESTAMP"
	echo ""
	echo "[NOTICE]"
	echo "${F_GREEN}PRE-EXISTING INSTALLATION DETECTED${NC}"
	echo ""
	
	echo "What would you like to do?"
	select MENU_CHOICE in "${INSTALLATION_MENU_01[@]}"
	do
		case $MENU_CHOICE in
			"Remove Install" )
				SYSTEM_CHECK="Yes"
				installation_check
				break;;
			"Info" )
				break;;
			"Exit" )
				install_manager_tmp_out
				break;;
		esac
	done
}

what_is_manager_02() {
	echo "Manager 2..."
}

gi_install() {
	cls
	echo "$TIMESTAMP"
	echo ""

	# Get the data from GitLab.
	get_git

	if [ -d "$GI_REPO" ]; then #1
		echo "[  ${F_GREEN}OK${NC}  ]"
		echo ""
		echo "Checking: $GI_VERSION"
	
		if [ -d "$GI_REPO_VERSION" ]; then #2
			echo "[  ${F_GREEN}OK${NC}  ]"
			echo ""
			echo "${F_YELLOW}Building local GeraltInstaller${NC}"
			gi_dirs
		
			if [ -d "$GI_LOC" ]; then #3
				echo "[  ${F_GREEN}OK${NC}  ]"
						
				if [ -d "$GI_LOCAL_VERSION" ]; then #4
					echo "[  ${F_GREEN}OK${NC}  ]"
							
					if [ -e "$GI_INSTALLER" ]; then #5
						echo "[  ${F_GREEN}OK${NC}  ]"
						echo ""
						echo "[ ${F_GREEN}Done${NC} ]"	
						echo ""
					else #5
						pause
						local_fail "gi_setup"
					fi #5
				else #4
					local_fail "gi_version"
				fi #4
			else #3
				local_fail "gi_home"
			fi #3
			
			pause
			cls
			
			if [ "$PRE_INSTALL_CHECK" == "NONE" ]; then #6
				echo "[${F_RED}Failed${NC}] Auto-run arugument not found."
				echo ""
				
				echo "${F_YELLOW}[NOTICE]"
				echo "No option to automatically run the setup"
				echo "and installation was detected...${NC}"
				echo ""
						
				echo "Would you like to run the setup and installation now?"
				select MENU_CHOICE in "${MENU_OPTIONS[@]}"
				do 
					case $MENU_CHOICE in
						"Yes" )
							clean_up
							install_manager_tmp_out
							$GI_LOCAL_VERSION/./$GI_SETUP
							break;;
						"No" )
							clean_up
							install_manager_tmp_out
							break;;
						"Maybe" )
							clean_up
							install_manager_tmp_out
							what_is_setup
							break;;
					esac
				done 
			elif [ "$PRE_INSTALL_CHECK" == "-a" ]; then #6
				clean_up
				install_manager_tmp_out
				echo "[  ${F_GREEN}OK${NC}  ] Auto-run detected."
				echo ""
				$GI_LOCAL_VERSION/./$GI_SETUP
			fi #6
		
		else #2
			install_manager_tmp_out
			repo_fail
		fi #2
		
	else #1
		install_manager_tmp_out
		repo_fail
		
	fi #1
}

gi_installation_manager_02() {
	cls
	echo "$TIMESTAMP"
	echo ""
	echo "[NOTICE]"
	echo "${F_YELLOW}NO PRE-EXISTING INSTALLATION DETECTED${NC}"
	echo ""
	
	echo "What would you like to do?"
	select MENU_CHOICE in "${INSTALLATION_MENU_02[@]}"
	do
		case $MENU_CHOICE in
			"Install" )
				gi_install
				break;;
			"Info" )
				what_is_manager_02
				break;;
			"Exit" )
				install_manager_tmp_out
				break;;
		esac
	done
}

# Tells the script which menu to call.
system_check() {
	echo "Checking for existing installation..."
	echo ""

	if [ -d "$GI_LOC" ]; then
		echo "[  ${F_GREEN}FOUND${NC}  ] Installed"
		echo ""
		gi_installation_manager_01
	else
		echo "[${F_RED}NOT FOUND${NC}] Not Installed"
		echo ""
		gi_installation_manager_02
	fi
}

system_check
